from importlib import import_module

import pytest
from unittest import mock


try:
    from pip import main as pipmain
except ImportError:
    from pip._internal import main as pipmain

from sequel.io import StorageFactory


def test_plugins_when_import_fails():
    def raise_importerror():
        raise ImportError

    entrypoints = [
        EntryPoint(
            "gs", raise_importerror
        ),
        EntryPoint(
            "other", lambda: object
        )
    ]

    with mock.patch("pkg_resources.iter_entry_points") as iter_entry_points:
        iter_entry_points.return_value = iter(entrypoints)

        storage_factory = StorageFactory()
        storage_factory.load_plugins("irrelevant entrypoint group")

    assert "gs" not in storage_factory._url_schemes_to_storage
    assert "other" in storage_factory._url_schemes_to_storage


class EntryPoint(object):
    def __init__(self, name, load):
        self.name = name
        self.load = load


@pytest.fixture()
def installed_module(temp_dir):
    root_dir = temp_dir / 'funky'
    module_file = root_dir / 'funkyscheme' / '__init__.py'
    setup_file = root_dir / 'setup.py'

    root_dir.mkdir()
    module_file.parent.mkdir()
    module_file.write_text(storage_distribution_module)
    setup_file.write_text(storage_distribution_config)

    install(str(root_dir))
    module = import_module('funkyscheme')
    yield module
    uninstall('funkyscheme')


def install(package):
    return pipmain(['install', '-q', package])


def uninstall(package):
    return pipmain(['uninstall', '-q', '-y', package])


storage_distribution_config = """
from setuptools import setup, find_packages

setup(
    name='funkyscheme',
    packages=find_packages(),
    entry_points={
        'sequel_storage': [
            'funkyscheme = funkyscheme:FunkySchemeStorage',
        ],
    }
)
"""


storage_distribution_module = """
from sequel.io import Storage

class FunkySchemeStorage(Storage):
    def write(self, text):
        pass

    def read(self):
        pass

def load_me():
    return FunkySchemeStorage
"""
