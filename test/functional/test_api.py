def test_api_objects_importable_from_package():
    from sequel import Chronicle  # noqa


def test_error_objects_importable_from_package():
    from sequel import OutputMismatch  # noqa


def test_ledger_objects_importable_from_package():
    from sequel import Ledger  # noqa


def test_singleton_chronicle_objects_importable_from_package():
    from sequel import chronicle  # noqa
