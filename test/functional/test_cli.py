import inspect
import os
from pathlib import Path
from textwrap import dedent

import pytest
from click.testing import CliRunner

from sequel import cli


def test_sequel_provides_usage(cli_runner):
    result = cli_runner.invoke(cli.main)
    assert result.exit_code == 0, result.stderr
    assert 'Usage: sequel' in result.stdout, result.stdout


def test_sequel_play_on_pyfile(graph_file, cli_runner):
    def job():
        print('find me')

    graph_file.jobs['graph_name'] = [job]
    graph_file.write()

    result = cli_runner.invoke(cli.occur, [str(graph_file.path)])

    assert result.exit_code == 0, result.exc_info
    assert 'find me' in result.stdout, result.stdout


class GraphFile(object):
    def __init__(self, path):
        self.path = path
        self.jobs = {}

    def write(self):
        content = [
            'from sequel import chronicle',
            '',
        ]

        for graph, funcs in self.jobs.items():
            for func in funcs:
                content.append('@chronicle.episode()'.format(graph))
                func_source = dedent(inspect.getsource(func))
                content.extend(func_source.splitlines())
                content.append('')

        with self.path.open('w') as opened:
            opened.writelines(line + os.linesep for line in content)


@pytest.fixture()
def cli_runner(chronicle_singleton):
    runner = CliRunner(mix_stderr=False)
    return runner


@pytest.fixture()
def graph_file(tmpdir):
    folder = Path(str(tmpdir))
    filepath = Path(folder, 'ledger.py')

    return GraphFile(filepath)
