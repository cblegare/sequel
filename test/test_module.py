import pytest

from sequel.codex import load_module


def test_inexistant_module_will_raise(inexistant_module):
    with pytest.raises(FileNotFoundError):
        _ = load_module(inexistant_module)


def test_invalid_module_will_raise(invalid_module):
    with pytest.raises(SyntaxError):
        _ = load_module(invalid_module)


@pytest.fixture()
def inexistant_module():
    return 'inexistant_module.py'


@pytest.fixture()
def invalid_module(tmpdir, file_maker):
    py_code = [
        '$% INVALID PYTHON CODE'
    ]
    module = file_maker(tmpdir, 'episode.py', py_code)
    return module


@pytest.fixture()
def irrelevant_module(tmpdir, file_maker, episode_module_lines):
    module = file_maker(tmpdir, 'episode.py', episode_module_lines)
    return module
