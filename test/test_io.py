from collections import OrderedDict
from pathlib import Path

import pytest

from sequel.ext.gcp import GoogleCloudStorage
from sequel.io import Storage, StorageFactory, FileSystemStorage


def test_loaders_uses_scheme(sample_uri_definition):
    class FirstIrrelevantStorage(Storage):
        def write(self, text):
            pass

        def read(self):
            pass

    class SecondIrrelevantStorage(Storage):
        def write(self, text):
            pass

        def read(self):
            pass

    url = 'first://host/path'

    factory = StorageFactory()

    factory.register('first', FirstIrrelevantStorage)
    factory.register('second', SecondIrrelevantStorage)

    storage = factory.make_storage(url)
    assert storage.url == url
    assert isinstance(storage, FirstIrrelevantStorage)


def test_filesystem_write(temp_dir):
    storage_path = temp_dir / 'some_filename'

    stored_content = 'unicode content'

    storage = FileSystemStorage(storage_path.as_uri())
    storage.write(stored_content)

    assert storage_path.read_text() == stored_content


def test_filesystem_read(temp_dir):
    storage_path = temp_dir / 'some_filename'

    stored_content = 'unicode content'

    storage_path.write_text(stored_content)

    storage = FileSystemStorage(storage_path.as_uri())
    read_content = storage.read()

    assert read_content == stored_content


def test_filesystem_filenotfound_resilient():
    storage_path = Path('some_non_existent_filename').absolute()

    storage = FileSystemStorage(storage_path.as_uri())
    read_content = storage.read()

    assert read_content == ''


uris = OrderedDict(
    gs={
        'url': 'gs://host/path',
        'storage_class': GoogleCloudStorage
    },
    file={
        'url': 'file://host/path',
        'storage_class': FileSystemStorage
    }
)


@pytest.fixture(params=uris.values(), ids=uris.keys())
def sample_uri_definition(request):
    return request.param
