import json

import pytest
import jsonschema

from sequel.ledger import Ledger, Outcome


def test_ledger_fails_with_cycle():
    with pytest.raises(ValueError):
        Ledger(
            {Outcome(1, 'now', 'value'), Outcome(2, 'now', 'value')},
            {1: {2},
             2: {1}}
        )


def test_ledger_tojson_from_json():
    ledger = Ledger(
        {Outcome(1, 'now', 'value'), Outcome(2, 'now', 'value')},
        {1: set(),
         2: {1}}
    )

    unserialized = Ledger.from_json(ledger.to_json())

    assert ledger.to_json(sort_keys=True
                          ) == unserialized.to_json(sort_keys=True)


def test_ledger_schema(ledger_json_schema):
    ledger = Ledger(
        {Outcome(1, '2019-04-03T15:49:48.739179+00:00', 'value'),
         Outcome(2, '2019-04-03T15:49:48.739179+00:00', {'somekey': 'value'})},
        {1: set(),
         2: {1}}
    )

    with ledger_json_schema.open() as opened:
        schema = json.load(opened)

    validator = jsonschema.Draft7Validator(schema)

    ledger_json = ledger.to_json()

    errors = list(validator.iter_errors(ledger_json))
    assert not errors


def test_outcome_equal():
    assert Outcome(1, 2, 3) == Outcome(1, 2, 3)
    assert Outcome(1, 0, 3) != Outcome(1, 2, 3)
    # value is not compared since we can't be sure it's comparable
    assert Outcome(1, 2, 0) == Outcome(1, 2, 3)
    assert Outcome(1, 2, 3) != 'something else'


@pytest.fixture()
def ledger_json_schema(project_root_dir):
    return project_root_dir / 'doc' / 'schemas' / 'ledger.json'
