import os

from pathlib import Path
import pytest


@pytest.fixture()
def project_root_dir():
    directory = Path(__file__).parent

    while directory:
        if list(directory.glob('setup.cfg')):
            return directory
        directory = directory.parent

    raise Exception('You broke something badly')


@pytest.fixture()
def folder_maker(tmpdir, file_maker):
    def maker(**lines_of_files):
        folder = Path(str(tmpdir))

        files = []

        for filename, lines in lines_of_files.items():
            filepath = file_maker(folder, filename, lines)
            files.append(filepath)
        return folder, files

    return maker


@pytest.fixture()
def chronicle_singleton():
    from sequel import chronicle
    yield chronicle


@pytest.fixture()
def file_maker():
    def maker(folder, filename, lines):
        filepath = Path(str(folder), filename)
        with filepath.open('w') as opened:
            opened.writelines(line + os.linesep for line in lines)

        return filepath

    return maker


@pytest.fixture()
def temp_dir(tmpdir):
    return Path(str(tmpdir))
