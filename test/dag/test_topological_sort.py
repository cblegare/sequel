from collections import OrderedDict

import pytest

from sequel.algo import topological_sort


def test_simple_topological_sort(some_graph):
    result = topological_sort(some_graph['dag'])

    assert list(result) == some_graph['expected']


def test_raise_with_cycle():
    graph = {1: {2},
             2: {1}}

    with pytest.raises(ValueError):
        list(topological_sort(graph))


def test_initial_left_intact():
    a_graph = {2: {11},
               9: {11, 8},
               10: {11, 3},
               11: {7, 5},
               8: {7, 3}}
    orig = a_graph.copy()
    _ = list(topological_sort(a_graph))
    assert a_graph == orig


o1 = object()
o2 = object()
o3 = object()
o4 = object()
o5 = object()


simple_graph = {1: {2, 3},
                2: {4}}

simple_expected = [{4, 3},
                   {2},
                   {1}]

samples = OrderedDict(
    simple={
        'dag': simple_graph,
        'expected': simple_expected},
    simple_string={
        'dag': {'1': {'2', '3'},
                '2': {'4'}},
        'expected': [{'4', '3'},
                     {'2'},
                     {'1'}]},
    simple_objects={
        'dag': {o1: {o2, o3},
                o2: {o4}},
        'expected': [{o4, o3},
                     {o2},
                     {o1}]},
    not_simple={
        'dag': {2: {11},
                9: {11, 8},
                10: {11, 3},
                11: {7, 5},
                8: {7, 3}},
        'expected': [{3, 5, 7},
                     {8, 11},
                     {2, 9, 10}]},
    self_dependencies_ignores={
        'dag': {2: {2, 11},
                9: {11, 8},
                10: {10, 11, 3},
                11: {7, 5},
                8: {7, 3}},
        'expected': [{3, 5, 7},
                     {8, 11},
                     {2, 9, 10}]},
    one_node={
        'dag': {1: set()},
        'expected': [{1}]},
    one_node_self_dependency={
        'dag': {1: {1}},
        'expected': [{1}]},
    empty={
        'dag': {},
        'expected': []},
)


@pytest.fixture(params=samples.values(), ids=samples.keys())
def some_graph(request):
    return request.param
