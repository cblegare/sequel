from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

from unittest.mock import create_autospec, MagicMock
import pytest

from sequel.dag import Chronicle


def test_simple():
    graph = Chronicle()

    def add(a, b):
        return a + b

    def sub(d, a):
        return d - a

    def div(c):
        return c / 10.

    graph.make_nodes(add, outcomes=('c',))
    graph.make_nodes(sub, outcomes=('e',))
    graph.make_nodes(div, outcomes=('d',))

    a_value = 2
    b_value = 3

    graph.continued({'a': a_value, 'b': b_value})

    expected_e = ((a_value + b_value) / 10) - a_value

    assert graph.data['e'] == expected_e


def test_teardown():
    graph = Chronicle()

    class CustomException(Exception):
        pass

    def tear_me_down():
        yield 1
        raise CustomException

    graph.make_nodes(tear_me_down)

    graph.continued()

    assert graph.data['tear_me_down'] == 1

    with pytest.raises(CustomException):
        graph.rollback()


def test_teardown_as_rollback():
    graph = Chronicle()

    class CustomException(Exception):
        pass

    def do_me_first():
        pass

    def tear_me_down(do_me_first):
        yield 1
        raise CustomException

    def do_me_last(tear_me_down):
        raise Exception

    graph.make_nodes(do_me_first)
    graph.make_nodes(tear_me_down)
    graph.make_nodes(do_me_last)

    with pytest.raises(CustomException):
        graph.continued()


def test_name_as_output():
    graph = Chronicle()

    def one():
        return 1

    def two():
        return 2

    def add(one, two):
        return one + two

    graph.make_nodes(one)
    graph.make_nodes(two)
    graph.make_nodes(add)

    graph.continued()

    assert graph.data['one'] == 1
    assert graph.data['two'] == 2
    assert graph.data['add'] == 1 + 2


def test_multiple_outcomes():
    def vector():
        return 1, 2, 3

    mocked_vector_to_count_calls = MagicMock(spec=vector,
                                             return_value=(1, 2, 3))
    # We need to trick inspect here
    from inspect import Signature
    mocked_vector_to_count_calls.__signature__ = Signature.from_callable(
        vector)

    chronicle = Chronicle()
    chronicle.make_nodes(mocked_vector_to_count_calls,
                         outcomes=('x', 'y', 'z'))
    chronicle.continued()

    assert mocked_vector_to_count_calls.call_count == 1


def test_recurence_are_resolved_again_even_in_data():
    chronicle = Chronicle()

    def recurrent():
        return 3

    mocked_recurrent_to_count_calls = create_autospec(recurrent,
                                                      return_value=3)
    # We need to trick inspect here
    from inspect import Signature
    mocked_recurrent_to_count_calls.__signature__ = Signature.from_callable(
        recurrent)

    def double(recurrent):
        return recurrent * 2

    chronicle.make_nodes(double)
    chronicle.make_nodes(mocked_recurrent_to_count_calls, recur=True)

    chronicle.continued({'recurrent': 1})
    assert mocked_recurrent_to_count_calls.call_count == 1

    chronicle.continued()
    assert mocked_recurrent_to_count_calls.call_count == 2


def test_recurence_multiple_outcomes_called_once():
    chronicle = Chronicle()

    def recurrent():
        return 3, True

    mocked_recurrent_to_count_calls = create_autospec(recurrent,
                                                      return_value=(3, True))
    # We need to trick inspect here
    from inspect import Signature
    mocked_recurrent_to_count_calls.__signature__ = Signature.from_callable(
        recurrent)

    chronicle.make_nodes(mocked_recurrent_to_count_calls,
                         recur=True,
                         outcomes=('number', 'some_bool'))

    chronicle.continued({'number': 1, 'some_bool': False})
    assert mocked_recurrent_to_count_calls.call_count == 1

    chronicle.continued()
    assert mocked_recurrent_to_count_calls.call_count == 2


def test_reuse_stored_data():
    chronicle = Chronicle()

    def one():
        return 1

    mocked_one_to_count_calls = create_autospec(one, return_value=1)
    # We need to trick inspect here
    from inspect import Signature
    mocked_one_to_count_calls.__signature__ = Signature.from_callable(one)

    def double(one):
        return one * 2

    chronicle.make_nodes(mocked_one_to_count_calls)
    chronicle.make_nodes(double)

    chronicle.continued({'one': 1})

    assert mocked_one_to_count_calls.call_count == 0
    assert chronicle.data['double'] == 2


def pickleable_function():
    return 1


def test_pickle():
    graph = Chronicle()

    graph.make_nodes(pickleable_function)

    import pickle

    pickled = pickle.loads(pickle.dumps(graph))

    pickled.continued()
    assert pickled.data['pickleable_function'] == 1


def test_alternate_executor(executor):
    graph = Chronicle(executor_class=executor)

    graph.make_nodes(pickleable_function)

    _ = graph.continued()


@pytest.fixture(params=[ProcessPoolExecutor,
                        ThreadPoolExecutor],
                ids=[ProcessPoolExecutor.__name__,
                     ThreadPoolExecutor.__name__])
def executor(request):
    return request.param
