Maintainer
==========

`@cblegare`_

.. _@cblegare: https://gitlab.com/cblegare

Contributors
============

None yet...

Special Thanks
==============

Thanks to veelox_, the original owner of the `sequel` name on PyPI_, for
letting us us this package name!

.. _veelox: https://pypi.org/user/veeloox/
.. _PyPI: https://pypi.org/
