.. _usage:

#####
Usage
#####

|sequel| builds a **chronicle** by reading **episodes** from a **codex**,
plays the **chronicle** in order and stores the **outcomes** to a **ledger**.

Using |sequel| is simple but may not be easy.  The reason is that |sequel|
won't do much for you.

The primary use case for |sequel| is to provide an imperative alternative to
declarative infrastructure as code frameworks (such as Terraform_ or Ansible_)
that will never let you down when faced with a non-trivial real world change
management situation.

To learn more about how |sequel| compares to other tools,
see :ref:`comparison`.

.. versionchanged:: 0.2
    The API was completely refactored


Getting started
===============

Writing a Chronicle
-------------------

The **codex** is a python file.  **episodes** are registered callable objects.

.. code-block:: python
    :caption: codex.py

    from subprocess import run
    from sequel import chronicle

    @chronicle.episode()
    def gke_cluster(existing_project_name):
        name = "mycluster"
        zone = "northamerica-northeast1-c"

        run([
            "gcloud", "container", "clusters", "create",
            name,
            "--project", existing_project_name,
            "--zone", zone
            "--quiet"
        ])

    @chronicle.episode()
    def existing_project_name():
        return 'myproject'


The :meth:`sequel.Chronicle.episode` decorator in a thin wrapper around the
:meth:`sequel.Chronicle.make_nodes` method that registers work into the graph.

Then, use the command line interface :program:`sequel` to sort and run that
set of interdependent jobs with the ``play`` command:

.. code-block:: shell

    $ sequel occur codex.py

Et voilà!


Cleaning Up
-----------

Just like `pytest`, |sequel| provides a way to cleanup episodes.

This may happen

* when something goes wrong is following episodes

* when called explicitly

The syntax is simple.  Instead of defining a simple function, define
a generator using ``yield`` instead of ``return``:

.. code-block:: python

    from sequel import chronicle

    @chronicle.episode()
    def tear_me_down():
        print("During resolution!")
        yield "some value"  # the tricks is HERE!
        print("During teardown!")

Et voilà!  Your episode can now be teared down.  If

.. code-block:: python
    :caption: codex.py
    :emphasize-lines: 18

    from subprocess import run
    from sequel import chronicle

    @chronicle.episode()
    def gke_cluster():
        name = "mycluster"
        project = "someproject"
        zone = "northamerica-northeast1-c"

        run([
            "gcloud", "container", "clusters", "create",
            name,
            "--project", project,
            "--zone", zone
            "--quiet"
        ])

        yield  # HERE!

        run([
            "gcloud", "container", "clusters", "delete",
            name,
            "--project", project,
            "--zone", zone
            "--quiet"
        ])

    @chronicle.episode()
    def deployed_app(gke_cluster):
        # kubectl or helm or whatever
        pass

The code after the ``yield`` statement in ``gke_cluster`` is the teardown
code.  This meas that ``gke_cluster`` episode will be rolled back if
``deployed_app`` raises an exception unless
:paramref:`sequel.Chronicle.continued.rollback` is False.

To disable this from your Codex, set ``ROLLBACK = False``


Command Line Interface
======================

The full synopsis for the command line interface is

.. click:: sequel.cli:main
    :prog: sequel
    :show-nested:


Recording Your Story in a Ledger
================================

|sequel| can store all **outcomes** in a **ledger**.  When re-run, the
**episodes** yielding stored **outcomes** do not occur again.

To do so, define a **LEDGER** attribute with our storage URL.

.. code-block:: python
    :caption: codex.py

    from sequel import chronicle

    LEDGER = 'my_ledger.json'

    @chronicle.episode()
    def my_firstname():
        return 'Charles'

    @chronicle.episode()
    def greeting(my_firstname):
        return 'Hello {!s}'.format(my_firstname)

As per :ref:`adr002`, the ledger is a json file.  Supported protocols are
``file://`` and ``gs://`` (Google Cloud Storage).


Storing a Ledger Elsewhere
--------------------------

.. versionadded:: 0.2

|sequel| provides a way to be extended with new storage classes using
**pkg_resources** (:std:doc:`pkg_resources`).  Any installed package can
provide storage classes to |sequel|.

Here is an example storage class:

.. code-block:: python
    :caption: custom_storage.py

    from sys import stdout, stdin
    from sequel.io import Storage

    class STDIOStorage(Storage):
        # Inheritage from Storage is not mandatory.
        # Make sure you provide a __init__ method that
        # takes one string argument (the URL) and stores it in
        # self.url.

        def write(self, text):
            stdout.write(text)

        def read(self):
            return stdin.read()

Then, using **setuptools** (:std:doc:`setuptools`), add the following
entrypoint definition in the ``sequel_storage`` entrypoint group:

.. code-block:: ini
    :caption: setup.cfg

    [options.entry_points]
    sequel_storage =
        stdio = custom_storage:STDIOStorage

That being done, in any environment (virtual or not) where your package
is installed, |sequel| will automatically use **STDIOStorage** for any
**ledger** URL having the ``stdio`` scheme:

.. code-block:: python
    :caption: codex_using_custom_storage.py

    from sequel import chronicle

    LEDGER = 'stdio://whatever'

    @chronicle.episode()
    def awesome():
        return 'SEQUEL!'

Congrats!

Programmable Usage
==================

The main API starts with a Codex.  Use directly :class:`sequel.Codex`.


.. _Ansible: https://www.ansible.com/
.. _Terraform: https://www.terraform.io/
