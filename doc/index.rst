########
|sequel|
########

.. only:: html

    |home_badge| |doc_badge| |lic_badge| |pypi_badge| |downloads_badge|

    .. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
        :target: http://www.gnu.org/licenses/lgpl-3.0
        :alt: GNU Lesser General Public License v3, see :ref:`license`.

    .. |home_badge| image:: https://img.shields.io/badge/gitlab-cblegare%2Fsequel-orange.svg
        :target: https://gitlab.com/cblegare/sequel
        :alt: Home Page

    .. |pypi_badge| image:: https://badge.fury.io/py/sequel.svg
        :target: https://pypi.org/project/sequel/
        :alt: PyPI Package

    .. |downloads_badge| image:: https://img.shields.io/pypi/dm/sequel.svg
        :target: https://pypi.org/project/sequel/
        :alt: PyPI Downloads

    .. |doc_badge| image:: https://img.shields.io/badge/docs-sequel-blue.svg
        :target: https://cblegare.gitlab.io/sequel
        :alt: Home Page

**se·quel** /ˈsēkwəl/

    Something that takes place after or as a result of an earlier event.

    *"this encouragement to grow potatoes had a disastrous sequel some
    fifty years later"*

Browse also the `project's home`_.


.. only:: latex

    The latest version of this document is also available online in the Docs_.

    .. _Docs: https://cblegare.gitlab.io/sequel/


.. only:: html

    The latest version of this document is also available as a |pdflink|.


.. toctree::
    :maxdepth: 2

    usage
    about
    changelog
    authors
    license


======================
Technical Informations
======================

.. only:: html

    |build_badge| |cov_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/sequel/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/sequel/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/sequel/badges/master/coverage.svg
        :target: _static/embedded/coverage/index.html
        :alt: Coverage Report

Here lies guides and references for contributors as well as some general
references.

.. toctree::
    :maxdepth: 2

    contributing
    arch/index
    modules
    glossary
    release


.. only:: html

    ==================
    Tables and indices
    ==================

    Quick access for not so much stuff.

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`



.. _project's home: https://gitlab.com/cblegare/sequel/
