.. _about:

##############
About |sequel|
##############

|sequel| is born from being frustrated with traditional configuration
managemment and infrastructure orchastration tools like Terraform.

It is inspired by

* Airflow_ because it sits on DAGs

* `Pytest fixtures`_ since it injects value from your code to your code

* Database migration tools like Alembic_ because imperative and stateful

* UNIX philosophy: opinionated, text-based, simple

* Storytelling because it is the oldest trick for making abstract stuff
  understandable.


.. _Airflow: https://airflow.apache.org/
.. _Pytest fixtures: https://docs.pytest.org/en/latest/fixture.html
.. _Alembic: https://alembic.sqlalchemy.org/en/latest/


|sequel| will

    * orchestrate and schedule work to do

    * remember what was already done and start from there

    * let you use any API you want the way you want it

    * help you define infrastructure as code

|sequel| will not

    * wrap any third party API for you. 
    
      This means that if your infrastructure uses two different providers 
      (say AWS and GCP), |sequel| will be of no help to unify their API in one 
      standard.

      It also means that |sequel| will never introduce bugs between you and
      your cloud provider.

      Finally, it also mean that you can use any API in a |sequel| in a single 
      Codex.

|sequel| does qualify as **cloud agnostic** since it is 
**anything-you-can-think-of agnostic**.


.. _comparison:

How does |sequel| compares with...
==================================

...my favorite IaC framework
----------------------------

It is imperative.  There are so many things that cannot be simply
declared!  Here are a few things that |sequel| can do

* changing the machine types of your Kubernetes cluster on GKE or AKS
  without downtime

* migrating an entire infrastructure from one provider to an other

* working around bugs in a cloud provider API (Azure anyone?!)

* using a mix of Terraform, Ansible, shell scripts and other magic
  to keep your infrastructure sane, clean and automatically continuously
  deployed.

Terraform will let you down in any of the above cases.


...this other workflow orchestrator
-----------------------------------

There are many workflow orchestrator such as Airflow. Here are some
reasons for using |sequel| instead.

* |sequel| is probably simpler to use from your CI pipeline (and simpler
  in general)

* |sequel| provide full control on which part is re-run on each deployment
