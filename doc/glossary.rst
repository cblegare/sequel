:orphan:

.. _glossary:

Glossary
========

.. glossary::
    :sorted:

    path-like

        See https://docs.python.org/3/glossary.html#term-path-like-object.

    picklable

        A picklable object can be serialized by :mod:`pickle`.  See also
        https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled
        for more information.
