.. _release:

Releaseing cheat sheet
======================

Is there a better time for writing down a standard release procedure than
before the first release?  Probably.

#.  Do not release by hand on your workstation

#.  Releases happens when a commit is tagged in the master branch of
    https://gitlab.com/cblegare/sequel.git.

#.  Work in clean environments (``rm -rf .tox``)

#.  Make sure CHANGELOG.rst is up-to-date with current changes.

#.  The standard build must be successful:

    #.  Tests are run for each required combination of configurations

    #.  Test coverage data are aggregated and reports are generated

    #.  Documentation is built using files from generated coverage reports
