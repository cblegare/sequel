import json
from pathlib import Path

import click


@click.command()
@click.argument('infile')
@click.argument('outfile')
def main(infile, outfile):
    infile = Path(infile)
    outfile = Path(outfile)

    with infile.open() as opened_infile:
        report = [json.loads(line)
                  for line in opened_infile.readlines()]

    with outfile.open('w') as opened_outfile:
        opened_outfile.write(json.dumps(report, indent='  '))


if __name__ == '__main__':
    main()
