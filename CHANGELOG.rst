Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_, and this project will adheres to
`Semantic Versioning`_ from version 1.0 and after.

..
    How do I make a good changelog?
    ===============================

    Guiding Principles
    ------------------

    - Changelogs are for humans, not machines.
    - There should be an entry for every single version.
    - The same types of changes should be grouped.
    - Versions and sections should be linkable.
    - The latest version comes first.
    - The release date of each version is displayed.
    - Mention whether you follow Semantic Versioning.

    Types of changes
    ----------------

    - **Added** for new features.
    - **Changed** for changes in existing functionality.
    - **Deprecated** for soon-to-be removed features.
    - **Removed** for now removed features.
    - **Fixed** for any bug fixes.
    - **Security** in case of vulnerabilities.

    [1.0.0] - 2017-06-20
    --------------------

    Added
    ~~~~~

    - Added a feature.


.. _release-next:

[unreleased]
------------

Nothing yet.


.. _release-0.4.2:

[0.4.2] - 2019-05-14
--------------------

Fixed
~~~~~

- More reliable teardown
- Noisy failure when something goes wrong


.. _release-0.4.1:

[0.4.1] - 2019-05-07
--------------------

Fixed
~~~~~

- Changes were not merged :(


.. _release-0.4:
.. _release-hardcover:

[0.4] - 2019-05-07
------------------

Added
~~~~~

- An API for Codex and friends! `!10`_

.. _!10: https://gitlab.com/cblegare/sequel/merge_requests/10


.. _release-0.3:
.. _release-delorean:

[0.3] - 2019-05-07
------------------

Added
~~~~~

- Now with rollbacks! `!9`_

- A better usage example in the docs

.. _!9: https://gitlab.com/cblegare/sequel/merge_requests/9


.. _release-0.2.2:

[0.2.2] - 2019-05-01
---------------------

Fixed
~~~~~

- ``google-cloud-storage`` was always imported. `!8`_

.. _!8: https://gitlab.com/cblegare/sequel/merge_requests/8


.. _release-0.2.1:


[0.2.1] - 2019-04-23
---------------------

Fixed
~~~~~

- Ledger reading on GCP would fail if file not found instead of returning
  nothing.

- Outcomes were not saved in ledger.


.. _release-0.2:
.. _release-storyteller:

[0.2] - 2019-04-05
------------------

The **storyteller** release!

`Milestone 0.2`_

.. _Milestone 0.2: https://gitlab.com/cblegare/sequel/milestones/2

Changed
~~~~~~~

- The API was completely refactored.

Added
~~~~~

- State can now be saved and not run again: `!7`_

Removed
~~~~~~~

- Specifying graph name is now removed: `!6`_

Fixed
~~~~~

- Episodes with multiples outcomes would be run multiple times.

.. _!6: https://gitlab.com/cblegare/sequel/merge_requests/6
.. _!7: https://gitlab.com/cblegare/sequel/merge_requests/7


.. _release-0.1:

[0.1] - 2019-04-01
------------------

This is our first release!

Added
~~~~~

- Command line interface that loads a python file for resolving jobs out
  of callables.

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
