import os
import configparser
from pathlib import Path


def project_root_dir():
    directory = Path(__file__).parent

    while directory:
        if list(directory.glob('setup.cfg')):
            return directory
        directory = directory.parent

    raise Exception('You break something badly')


def setup_config(project_root_dir):
    config = configparser.ConfigParser()
    config.read(str(project_root_dir / 'setup.cfg'))
    return config


def write_requirements_file(project_info, outputfile):
    deps = [dep.strip() + os.linesep
            for dep in project_info['options']['install_requires'].splitlines()
            if dep]

    with Path(outputfile).open('w') as opened:
        opened.writelines(deps)


def main():
    project_root = project_root_dir()
    project_info = setup_config(project_root)

    write_requirements_file(project_info, project_root / 'requirements.txt')


if __name__ == '__main__':
    main()
