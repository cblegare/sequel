######
sequel
######

Opinionated imperative Everything as Code orchestrator.

**se·quel** /ˈsēkwəl/

    Something that takes place after or as a result of an earlier event.

    *"this encouragement to grow potatoes had a disastrous sequel some
    fifty years later"*
